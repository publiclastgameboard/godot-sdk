# 1.0.0

- First release of Godot SDK. Includes controllers to interact with the Gameboard. See documentation [here](https://external-codelabs.hosting.lastgameboard.com/).
- Access the Gameboard node with `var gameboard = GetNode("/root/GameboardSDK") as GameboardPlugin;` to gain access to all the controllers.
